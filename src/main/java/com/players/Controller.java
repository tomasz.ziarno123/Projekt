package com.players;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class Controller {

    @Autowired
    private PlayersDao playersDao;

    @GetMapping("/player/getAll")
    public List<Model> getAllStudents() {

        return playersDao.getPlayers();

    }

    @GetMapping("/player/{id}")
    public ResponseEntity<Model> getStudentById(@PathVariable long id) {
        Model player = playersDao.findById((int) id);
        return ResponseEntity.ok(player);
    }

    @PostMapping("/player/save")
    public Model save(@RequestBody Model player) {
        return playersDao.save(player);
    }

    @PutMapping("/player/update/{id}")
    public Model update(@RequestBody Model player, @PathVariable String id) {
        Model playerReady = playersDao.findById(Integer.parseInt(id));
        playerReady.setFirstName(player.getFirstName());
        playerReady.setSecondName(player.getSecondName());
        playerReady.setBranch(player.getBranch());
        playerReady.setNationality(player.getNationality());
        playerReady.setAge(player.getAge());

        return playersDao.save(playerReady);
    }

    @DeleteMapping("/player/delete/{id}")
    public void delete(@PathVariable String id) {
        playersDao.deleteById(Integer.parseInt(id));
    }
}
