package com.players;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PlayersDao {
    @Autowired
    private Rep repository;

    public Model save(Model player) {
        return repository.save(player);
    }

    public void delete(Model player) {
        repository.delete(player);
    }


    public List<Model> getPlayers() {
        List<Model> players = new ArrayList<Model>();
        Streamable.of(repository.findAll()).forEach(players::add);
        return players;
    }

    public Model findById(int parseInt) {

        return repository.findById(parseInt).get();
    }

    public void deleteById(int parseInt) {
        repository.deleteById(parseInt);
    }

}