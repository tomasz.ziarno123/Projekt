package com.players;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface Rep extends CrudRepository<Model, Integer> {
}
